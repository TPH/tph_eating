--[[
    Input-held Eating Mod for Exile
    Copyright (C) 2024 TPH/TubberPupperHusker <damotrixrob@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]
-----------------------------------
-- check through game compatibilities before mod compatibilities
minetest.register_on_mods_loaded(function()
  local item_list = table.copy(minetest.registered_items)
  -- MOD SUPPORT FOR MANTAR'S EXILE

  if minetest.get_modpath("health") and minetest.get_modpath("minimal") then
    local is_v4 = minetest.get_modpath("mapchunk_shepherd") and true or false
    -- only v4 has the mapchunk shepherd
    if is_v4 then
      -- v4 SETTINGS -------------------------------
      tph_eating.use_function = "_on_use_item"
      tph_eating.use_key = "aux1"
      -------------------------------------------
    else -- v3
      HEALTH = HEALTH -- declare global
    end
    tph_eating.burping = false

    local look_through = is_v4 and HEALTH.food_table or food_table
    local edible = {}
    local function add_edible(name,def)
      if ((def.groups and def.groups.edible) or look_through[name] or
      (name == "tech:tiku" or name == "tech:herbal_medicine" or name == "tech:soup")) then
        if name == "tech:soup" or (def.mod_origin == "tech" and name:match("tang")) or
          def.groups and (def.groups.soup or def.groups.drink) then
          def = table.copy(def)
          def.sounds = def.sounds or {}
          def.sounds.eating_chew = table.copy(tph_eating.slurp_sound)
        end
        edible[name] = def
      end
    end
    for name,def in pairs(item_list) do
      add_edible(name,def)
      item_list[name] = nil -- remove from global list so that we are not reiterating in mod support
    end
    for name,def in pairs(edible) do
      local old_use_name = is_v4 and tph_eating.use_function or "on_use"
      local old_use = def[old_use_name]
      local modify_old_use = false
      -- used for snow lol
      if is_v4 and name == "nodes_nature:snow" then
        modify_old_use = function(player, wielded_item, pointed_thing)
          if pointed_thing and pointed_thing.type == "node" then
            return minimal.slabs_combine(player, wielded_item, pointed_thing,
            "nodes_nature:snow_block")
          elseif not tph_eating.get_player_eating_data(player) then
            tph_eating.eating_func(player, wielded_item)
          end
        end
      end
      minetest.override_item(name,{
        [old_use_name] = modify_old_use,
        tph_eating_success = function(player, itemstack)
          if is_v4 and def.groups then
            -- run on_use for poisonous plants
            if string.match(name,"nebiyi") then
              return old_use(player,itemstack)
            elseif string.match(name,"marbhan") then
              return old_use(player,itemstack)
            end
            -- does not come with a predetermined on_use function, act as one
            if def.groups.edible == 2 then
              return HEALTH.eatdrink_playermade(itemstack, player)
            else
              return HEALTH.eatdrink(itemstack, player)
            end
          end
          -- v3 will run this
          return old_use(itemstack, player, {type="object",ref=player})
        end,
        sounds = def.sounds,
      })
      tph_eating.add_eating_hook(name,is_v4 and not modify_old_use)
    end
---------------------------------------------------------------------------------------------------------
  -- MINETEST GAME COMPATIBILITY

  elseif minetest.get_modpath("default") and minetest.get_modpath("farming") and minetest.get_modpath("flowers") then
    local consumables = {"default:apple","default:blueberries","farming:bread","flowers:mushroom_red"}
    for _,name in pairs(consumables) do
      tph_eating.on_use_override(name,true)
      item_list[name] = nil
    end
    -- silly
    if tph_eating.silly == true then
      local silly_consume = {
        ["default:diamond"] = 10,
        ["default:gold_ingot"] = 5,
        ["default:mese_crystal"] = 15
      }
      for name,amt in pairs(silly_consume) do
        local def = minetest.registered_items[name]
        if def then
          minetest.override_item(name,{
            on_use = false,
            tph_eating_success = function(player, itemstack)
              return minetest.item_eat(amt)(itemstack, player)
            end
          })
          tph_eating.add_eating_hook(name)
          item_list[name] = nil
        end
      end
    end
  end
---------------------------------------------------------------------------------------------------------
  -- MOD COMPATIBILITIES

  -- "Your Dad's BBQ Mod", I have a passion against you for having absolutely no helpful groups
  -- I HAD TO FIX ACCIDENTALLY DRINKING A SPATULA!... A SPATULA!!! OH MY GOD
  local function is_liquid_or_solid(name,def)
    -- MODS, WHY YOU NO USE GROUPS?
    local ungrouped_list = {
      -- ethereal... wilhelmine's natural biomes... 3:<
      -- TENPLUS1!!! WHY!
      solid = {"banana","orange","nut","berry","olive","lemon","apple","stew","jelly","sushi","fugu","teriyaki","shrimp","calamari",
        "fish","sashimi","tuber","healing","onion","yellowleaves","fried","honey","root","pine","rose","humanmeat","salad",
        "edamame","corn","bees:honey_comb","sandwich","delight","garlic","donut","jaffa","pie","cheese","spaghetti",
        "bibimbap","burger","potato","paella","vanilla","onigiri","gyoza","mochi","ginger","carrot","cookie","chili",
        "chocolate","bread","bagel","muffin","candycane"},
      -- CUCINA VEGANA WHY!!!!!!
      liquid = {"milk","cup","hollandaise","soup","potion","bottle_honey","beer","porridge","tea","fondue","puree",
      "sauce","jcu_","latte"}
    }
    -- basically checks if an item has a specified group that contains "food" in its name
    local function has_food_group()
      for gname,_ in pairs(def.groups) do
        if type(gname) == "string" and gname:match("food") then
          -- add type check in case a programmer makes a numbered group for some reason (is that possible???)
          return true
        end
      end
    end
    -- contains a string in the name that's considered a food
    local function is_food_named()
      if def.mod_origin == "jelys_pizzaria" and name:match("slice") then
        -- why no pizza slice group???
        return true
      elseif def.mod_origin == "bbq" and (name:match("sugar") or not def.groups.vessel) then
        return true
      elseif def.mod_origin == "large_slugs" and string.match(name,"cooked") then
        return true
      elseif def.mod_origin == "pumpkinspice" and string.match(name,"cake") then
        return true
      elseif def.mod_origin == "icecream" then
        return true
      -- fishing rod, don't eat this lol
      elseif name:match("fish") and name:match("rod") then
        return false
      else
        for _,ungrouped in pairs(ungrouped_list.solid) do
          -- matches predetermined list of names
          if name:match(ungrouped) then
            return true
          end
        end
      end
    end
    -- contains a string in the name that's considered a drink
    local function is_drink_named()
      if def.mod_origin == "cucina_vegana" and def.groups.honey then
        -- not every honey related thing is drinkable, but cucina vegana certainly makes that harder 3;<
        return true
      elseif def.mod_origin == "bbq" and def.groups.vessel then
        if name:match("brush") or name:match("spatula") or name:match("sugar") then
          return
        end
        return true
      elseif def.mod_origin == "pumpkin_pies" and name:match("mix") then
        return true
      else
        for _,ungrouped in pairs(ungrouped_list.liquid) do
          -- matches predetermined list of names
          if name:match(ungrouped) then
            return true
          end
        end
      end
    end
    def.groups = def.groups or {} -- minetest doesn't fill in with an empty groups table.......
    local grps = def.groups
    if grps.drink or grps.food_milk or grps.food_milk_glass or grps.food_coconut_milk or grps.food_water or
    grps.food_mayonnaise or grps.food_soup
    -- Wuzzy why you no groups AAAA
    or def.mod_origin == "pep"
    -- mods don't specify groups... EVEN FOR LIQUIDS?
    or is_drink_named() then
      return true
    elseif not (grps.food_egg and not name:match("fried")) and (grps.food or grps.meat or grps.eatable
    -- mods don't specify groups sometimes... UGH
    or is_food_named()
    -- or it's too excessive and specific...
    or has_food_group()) then
      return false
    else
      return nil
    end
  end
  -- adding edible foods from mods to conversion list
  local mod_edible = {}
  local function mod_add_edible(name,def)
    if type(def.on_use) == "function" then
      local liquid = is_liquid_or_solid(name,def)
      if liquid then
        def = table.copy(def)
        def.sounds = def.sounds or {}
        def.sounds.eating_chew = table.copy(tph_eating.slurp_sound)
        mod_edible[name] = def
      elseif liquid == false then
        mod_edible[name] = def
      end
    end
  end
  for name,def in pairs(item_list) do
    mod_add_edible(name,def)
  end
  for name,def in pairs(mod_edible) do
    tph_eating.on_use_override(def,true)
  end
end)