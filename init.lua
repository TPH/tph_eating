--[[
    Input-held Eating Mod for Minetest
    Copyright (C) 2024 TPH/TubberPupperHusker <damotrixrob@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]
local v560 = minetest.features and (minetest.features.particlespawner_tweenable or minetest.features.get_sky_as_table or minetest.get_tool_wear_after_use) and true -- use new particle spawner features (5.6.0) or legacy
local v540 = minetest.features and (minetest.features.direct_velocity_on_players and minetest.features.use_texture_alpha_string_modes) and true -- eating_item_entity possible or false
tph_eating = {
  eating_time = 0.36,
  eating_repeats = 4,
  use_key = "RMB", -- SHOULD BE STRING TO INDEX
  use_function = "on_secondary_use", -- SHOULD BE STRING TO INDEX
  entity_use_function = "on_rightclick", -- SHOULD BE STRING TO INDEX ON ENTITES
  burping = true,
  burp_chance = 100,
  silly = false,
  eating_item_entity = true,
}
tph_eating.eating_sound = {
  name = "tph_eating_chew",
  gain = {0.35,0.45},
  pitch = {0.85,1.04}
}
tph_eating.slurp_sound = {
  name = "tph_eating_slurp",
  gain = {0.25,0.3},
  pitch = {0.95,1.1}
}
tph_eating.finished_sound = {
  name = "tph_eating_burp",
  gain = 0.35,
  pitch = {0.93,1.03}
}

-- MISC API \/
tph_eating.creative_mode = minetest.settings:get_bool("creative_mode")
-- custom function for detecting if player is in creative
tph_eating.player_in_creative = function(player)
  -- get the player by name if string
  player = type(player) == "string" and core.get_player_by_name(player) or player
  -- if player is a player...
  if core.is_player(player) then
    if minetest.check_player_privs(player,"creative") or tph_eating.creative_mode == true then
      return true
    end
  end
  return false
end

-- for getting an edible item's definition
-- uses get_definition() for itemstacks
local function get_def(item)
  -- ensure getting a proper table lol
  item = type(item) == "table" and item.name or item
  -- return itemstack's get_definition
  if type(item) == "userdata" and type(item.get_definition) == "function" then
    return item:get_definition()
  end
  -- couldn't get a string to check
  if type(item) ~= "string" then return end
  return core.registered_items[item]
end

local function play_sound(sound_def,pos)
  if type(sound_def) ~= "table" then
    -- you gave me something that doesn't work
    error(debug.traceback("tph_eating.play_sound: provided sound_def is not a table! Got type: "..type(sound_def),2))
  end
  local sound_name = sound_def.name -- # permits using a table as so: sound_table = {name = "sound", 1 = {stuff}, 2 = {stuff} }
  -- if more than 1 playable sound that is
  if #sound_def > 1 then
    -- more than 1 sound to play
    local playable_sounds = {}
    for _,sound in pairs(sound_def) do
      if type(sound) == "table" and (sound.name or sound_name) then
        playable_sounds[#playable_sounds + 1] = sound
      end
    end
    if #playable_sounds <= 0 then
      -- did not get proper sound defs
      error(debug.traceback("tph_eating.sound_play: more than 1 sound provided in sound_def, however no proper sound defs inside!",2))
    end
    sound_def = playable_sounds[math.random(1,#playable_sounds)]
    sound_name = sound_def.name or sound_name
  end
  if type(sound_name) ~= "string" then
    -- no name
    error(debug.traceback("tph_eating.sound_play: improper name provided for sound!",2))
  end
  sound_name = string.gsub(sound_name," ","") -- remove spaces
  if sound_name == "" then -- you didn't want to play this sound
    return -1
  end
  sound_def = table.copy(sound_def) -- prevent overwriting tables
  sound_def.name = sound_name -- apply modified name rid of any errors
  -- verify or eliminate pos
  if type(pos) == "table" then
    pos = {x = tonumber(pos.x), y = tonumber(pos.y), z = tonumber(pos.z)}
    if not pos.x or not pos.y or not pos.z then
      pos = nil
    end
  end
  if type(pos) == "table" then
    sound_def.pos = pos
  elseif type(pos) == "userdata" then
    sound_def.object = pos
  end
  -- utilize singular number or randomized value
  local function get_range(value)
    -- if value is a table and its index 1 and 2 are numbers then return a randomized value between them, otherwise return value
    return type(value) == 'table' and tonumber(value[1]) and tonumber(value[2]) and
    (value[1]+math.random()*(value[2]-value[1]) ) or value
  end
  -- automatically set each number table value (in case of future new values)
  for valuename,value in pairs(sound_def) do
    if type(value) == "table" and tonumber(value[1]) and tonumber(value[2]) then
      sound_def[valuename] = get_range(value)
    end
  end

  return minetest.sound_play(sound_def.name,sound_def)
end
tph_eating.sound_play = play_sound

-- gets tph_eating-based information of an ItemStack
-- randomizes sound data according to provided ItemStack parameters and tph_eating settings
-- returns 4 values utilize by the eating function
local function get_eating_information(item, noerror)
  item = type(item) == "table" and item or get_def(item)
  if not item and not noerror then
    error("tph_eating: Could not get eating information for "..tostring(item))
  end
  local eating_time = math.max(tph_eating.eating_time,0.001) -- minimum of 1 millisecond
  local eating_repeats = tonumber(item.tph_eating_repeats) or tph_eating.eating_repeats
  eating_repeats = math.max(eating_repeats,2) -- minimum of 2 eating repeats
  local sounds = item.sounds 
  local eating_sound = tph_eating.eating_sound
  local finished_sound = tph_eating.finished_sound
  if sounds then
    eating_sound = (type(sounds.eating_chew) == "function" or type(sounds.eating_chew) == "table") and sounds.eating_chew or
    eating_sound or {}
    finished_sound = (type(sounds.eating_finished) == "function" or type(sounds.eating_finished) == "table") and
    sounds.eating_finished or finished_sound or {}
  end
  -- # accepts getting a sound table from a function or table
  if type(eating_sound) == "function" then
    eating_sound = eating_sound()
    if type(eating_sound) ~= "table" then
      eating_sound = {name = "tph_eating_chew"}
    end
  end
  if type(finished_sound) == "function" then
    finished_sound = finished_sound()
    if type(finished_sound) ~= "table" then
      finished_sound = {name = "tph_eating_burp"}
    end
  end
  local burp_chance = tph_eating.burp_chance/100
  -- you don't like my burping lol, let's modify eating_sound for a finished_sound if you didn't specify one
  -- also possible for random chance :D (suggested by j0j0n4th4n on minetest forums)
  if string.match(finished_sound.name,"burp") and (tph_eating.burping ~= true or math.random() > burp_chance) then
    finished_sound = {
      name = eating_sound.name,
      gain = eating_sound.gain,
      -- table, number, or nil - depends on eating_sound
      pitch = type(eating_sound.pitch) == "table" and {eating_sound.pitch[1]*0.78,eating_sound.pitch[1]*0.9} or
      type(eating_sound.pitch) == "number" and eating_sound.pitch * 0.8 or eating_sound.pitch
    }
  end

  return {
    eating_time = eating_time,
    eating_repeats = eating_repeats,
    eating_sound = eating_sound,
    finished_sound = finished_sound,
  }
end
tph_eating.get_eating_information = get_eating_information

-- local table to determine which players are eating
local players_eating = {}

-- get player eating data
local function get_eating_data(player)
  -- get string
  player = type(player) == "string" and player or minetest.is_player(player) and player:get_player_name()
  return players_eating[player]
end
tph_eating.get_player_eating_data = get_eating_data

local eating -- declare 'eating' so that 'clear_eating' can use it

-- remove player from table global of concurrent players indulging in food
local function clear_eating(player, hold)
  -- get string
  player = type(player) == "string" and player or minetest.is_player(player) and player:get_player_name()
  -- get player's eating data
  local data = get_eating_data(player)
  if not data then return end
  if type(data.obj) == "userdata" then -- eating_item_entity is or was enabled and the player has an eating object, get rid of
    -- ensure player object for hud edit
    local plr = minetest.get_player_by_name(player)
    if plr then
      -- return to prior wielditem hud state
      plr:hud_set_flags({wielditem = data.hud_wielditem or data.hud_wielditem ~= false and true})
    end
    data.obj:remove()
  end
  -- permit custom "hold" boolean to hold for a bit instead of deleting information
  -- only runs if iteration is over the eating repeats - and if both exist
  -- permits holding down use_key to eat again
  if hold and data.iteration and data.eating_info and data.eating_info.eating_repeats and data.iteration >= data.eating_info.eating_repeats then
    minetest.after(math.max(tph_eating.eating_time,0.001)*2, function()
      local plr = minetest.get_player_by_name(player)
      local data = get_eating_data(player)
      -- ensure we have plr AND data
      if plr and data then
        -- refresh some stats
        data.iteration = 1
        data.hud_wielditem = nil
        data.item_pos = nil
        data.image_list = nil
        -- run eating again, use plr and get their wielded_item
        eating(plr, plr:get_wielded_item())
      end
    end)
  -- remove eating information
  else
    players_eating[player] = nil
  end
end
-- allow mods to clear player eating
tph_eating.clear_eating = clear_eating

-- stop a player's nutritional indulgence
local function cease_eating(player)
  local data = get_eating_data(player)
  if data then
    data.force_finish = true
  end
end
-- allow mods to stop a player from eating
tph_eating.cease_eating = cease_eating

-- better automate compatibility supports
-- assumes eating function is on_use and that mod does not have any support of tph_eating
-- siic = "SaveItemStackInCreative" -- adds to the ItemStack prior to on_use function
local function automated_on_use_override(def,siic)
  -- will only accept valid definitions or their names (registered items)
  -- if a table;
  -- will only have an option to override sounds, tph_eating_image, and tph_eating_repeats - will not accept any other values
  def = type(def) == "table" and def or get_def(def)
  if not def then return end
  if def.groups and def.groups.tph_eating_no_edit then
    -- explicitly said no editing lol
    return
  end
  -- only override if on_use is specified
  if type(def.on_use) == "function" then
    local old_use = def.on_use
    minetest.override_item(def.name,{
      on_use = false, -- prevent usage of on_use
      tph_eating_success = function(player, itemstack)
        if siic and tph_eating.player_in_creative(player) then
          itemstack:set_count(itemstack:get_count()+1)
        end
        return old_use(itemstack, player, {type="nothing"})
      end,
      -- following will not do anything if nil
      sounds = def.sounds,
      tph_eating_image = def.tph_eating_image,
      tph_eating_repeats = def.tph_eating_repeats
    })
    tph_eating.add_eating_hook(def.name)
  end
end
tph_eating.on_use_override = automated_on_use_override
-- MISC API /\

-- PRIMARY FUNCTIONS \/

-- MAIN EATING FUNCTION
eating = function(player, itemstack)
  if type(itemstack) ~= "userdata" or not minetest.is_player(player) then
    -- you're no fun, you gave me a bad itemstack or player
    clear_eating(player)
    return
  end
  local pname = player:get_player_name()
  local data = players_eating[pname] or {}
  data.iteration = tonumber(data.iteration) or 1
  data.tool_def = data.tool_def or get_def(itemstack)
  if not data.tool_def then
    -- can't be eating with no tooldef
    clear_eating(player)
    return
  end
  -- tool def interactions
  -- images (set as table for more randomization/options)
  data.image_list = data.image_list or data.tool_def.tph_eating_image or data.tool_def.inventory_image
  data.image_list = data.image_list == "" and data.tool_def.tiles or data.image_list -- get node tiles if no inventory_image
  -- allow getting node tiles if gotten string (likely tph_eating_image) equals "tiles"
  data.image_list = type(data.image_list) == "table" and data.image_list or type(data.image_list) == "string"
    and (data.image_list == "tiles" and data.tool_def.tiles or {data.image_list}) or nil
  -- get a new image every iteration
  data.image = data.image_list and data.image_list[math.random(1,#data.image_list)] or nil
  data.image = type(data.image) == "table" and data.image.name or data.image -- get any provided texture
  --[[
  -- tool def interactions
  data.img = data.img or data.tool_def.tph_eating_image or data.tool_def.inventory_image
  if data.img == "" and data.tool_def.tiles then
    -- support for getting texture from node if inventory_image doesn't exist
    local tiles = data.tool_def.tiles
    data.img = tiles[math.random(1,#tiles)].name or tiles[math.random(1,#tiles)] -- get any provided texture
  end
  --]]
  -- eating_info for getting stable _eating values
  data.eating_info = data.eating_info or tph_eating.get_eating_information(data.tool_def)
  local eating_time = data.eating_info.eating_time
  local eating_repeats = data.eating_info.eating_repeats
  -- first time?
  if data.iteration == 1 then
    -- # clear_eating not necessary for returns since player has not yet been added to eating global
    -- custom function for specifying whether or not the eating should commence
    if type(data.tool_def.tph_eating_condition) == "function" and data.tool_def.tph_eating_condition(player, itemstack) ~= true then
      return
    end
    data.index = tonumber(data.index) or player:get_wield_index()
    -- allow for custom "eating_initiated" function to be defined upon eating start
    itemstack = type(data.tool_def.tph_eating_initiated) == "function" and data.tool_def.tph_eating_initiated(player, itemstack, data) or itemstack
    if type(itemstack) ~= "userdata" then
      -- no breaking allowed
      return
    end
  end
  local w_item = player:get_wielded_item()
  local control = player:get_player_control()
  -- ate item fully, no longer eating or selecting food item, or dropped item
  if (itemstack:get_name() == "" or itemstack:is_empty()) or not control[tph_eating.use_key] or
  not (data.index or data.index ~= player:get_wield_index()) or
  data.tool_def.name ~= w_item:get_name() or data.force_finish then
    -- custom failed eat function
    if type(data.tool_def.tph_eating_failed) == "function" then
      data.tool_def.tph_eating_failed(player, itemstack, data)
    end
    clear_eating(player)
    return
  else
    -- update itemstack to wielded item
    itemstack = w_item
  end
  local itr_pos = player:get_pos() -- interaction pos
  if type(data.height) ~= "number" then
    local collbox = player:get_properties().collisionbox
    data.height = (collbox[2] + collbox[5])*0.67 -- height for where particles should be (67% of total player height)
  end
  -- # height can be set in tph_eating_ongoing if player's true height changes (such as in a chair or bed)
  -- spawn an entity infront of the player when they're eating (don't run code if someone unregistered the entity 3:<)
  -- only works 5.4.0+ (entity doesn't get registered on older versions)
  if tph_eating.eating_item_entity == true and minetest.registered_entities["tph_eating:eating_item"] then
    local function create_obj()
      -- allow players to specify an entity override for what will be shown as the item during consumption instead
      local using_itemstack = data.tool_def.tph_eating_itemstack_entity_override
      if using_itemstack then
        using_itemstack = minetest.registered_items[using_itemstack] and using_itemstack or nil
      end
      using_itemstack = using_itemstack or itemstack:get_name()
      return minetest.add_entity({x=0,y=0,z=0},"tph_eating:eating_item",using_itemstack)
    end
    local function attach_obj()
      if data.obj then
        -- attach to player's "Head" bone, at item_pos, no rotation, allow being seen in first person
        data.obj:set_attach(player,"Head",data.item_pos,nil,true)
      end
    end
    -- add entity
    data.obj = data.obj or create_obj()
    data.item_pos = data.item_pos or {x=0,y=1,z=-3} -- modify this to create custom itemstack positions
    if data.obj then
      attach_obj()
      -- only run hud edit once
      if not data.hud_wielditem then
        data.hud_wielditem = data.hud_wielditem ~= false and player:hud_get_flags().wielditem
        player:hud_set_flags({wielditem = false}) -- make itemstack in hand disappear
      end
      -- chance of weird invisible item entity bug if this isn't done
      if not data.obj:get_pos() then
        data.obj:remove()
        data.obj = create_obj()
        attach_obj()
      end
    end
  end
  -- code for eating particles
  if type(data.image) == "string" then
    local prtc_pos = {x=0,y=data.height*0.96,z=0.5} -- particle position
    if data.obj then
      -- prioritize setting particles to item entity
      prtc_pos = {x=0,y=0,z=0}
    end
    local bounds = 4 -- crop image bounds (X,Y), should not be lower than 1
    local prtc_amt = 2
    if (data.iteration + 1) >= eating_repeats then
      -- create more particles upon finishing
      prtc_amt = 6
    end

    local prtc_def = {
      pos = prtc_pos,
      attached = data.obj or player,
      time = (eating_time), -- last as long as it takes for a player to eat again
      amount = prtc_amt,
      collisiondetection = true,
      collision_removal = true,
      -- negative Z goes forwards from player
      vel = {min={x = -1.5, y = 1, z = -2},max={x = 1.5, y = 3.5, z = 0}  },
      acc = {x = 0, y = -11, z = 0},
      size = {min=0.5, max=1},
      vertical = true -- face player
      --glow = 5 -- makes particles not so dark at night lol
    }
    if not v560 then
      -- compatibility for v5.0.1 to before v5.6
      prtc_def.minpos = prtc_pos
      prtc_def.maxpos = prtc_pos
      prtc_def.minvel = prtc_def.vel.min
      prtc_def.maxvel = prtc_def.vel.max
      prtc_def.minacc = prtc_def.acc
      prtc_def.maxacc = prtc_def.acc
      prtc_def.minsize = prtc_def.size.min
      prtc_def.maxsize = prtc_def.size.max
    end

    for i = 1, 6, 1 do
      -- random bounds
      -- subtract 1 to prevent error on MT 5.9
      local rbounds = {math.random(0,bounds-1),math.random(0,bounds-1)}
      prtc_def.texture = data.image.."^[sheet:"..tostring(bounds).."x"..tostring(bounds)..":"..rbounds[1]..","..rbounds[2]
      minetest.add_particlespawner(prtc_def)
    end
  end
  if data.iteration >= eating_repeats then
    -- custom eating success function, sounds will have to be played in function
    local tempstack = type(data.tool_def.tph_eating_success) == "function" and data.tool_def.tph_eating_success(player, itemstack, data) or nil
    itemstack = type(tempstack) == "userdata" and tempstack or itemstack -- ensure we get a proper itemstack in return
    play_sound(data.eating_info.finished_sound, player)
    player:set_wielded_item(itemstack)
    clear_eating(player, true)
    return
  else
    -- do not run custom function if began eating
    if data.iteraton ~= 1 then
      -- custom function "tph_eating_ongoing" to run each time it iterates over eating, expects itemstack in return or nil
      itemstack = type(data.tool_def.tph_eating_ongoing) == "function" and data.tool_def.tph_eating_ongoing(player, itemstack, data) or itemstack
      -- # could add a custom eating animation, get time with data.eating_time
    end
    play_sound(data.eating_info.eating_sound, player)
  end
  -- ending iteration
  data.iteration = data.iteration + 1
  players_eating[pname] = data
  minetest.after(eating_time, eating, player, itemstack)
end
-- permits mods to use the eating function instead of using an eating hook
tph_eating.eating_func = eating

-- primary function for determining whether or not to eat food
-- expects on_secondary_use arguments
local function on_main_use(itemstack, user, pointed_thing, oldfunc)
  if type(itemstack) ~= "userdata" or type(user) ~= "userdata" then
    -- you did something bad
    return itemstack
  end
  -- run old function if applicable
  local use_result = type(oldfunc) == "function" and oldfunc(itemstack, user, pointed_thing) or nil
  if minetest.is_player(itemstack) then -- itemstack arg is player
    local temp = user
    user = itemstack
    itemstack = temp
  end
  -- if old function returns something that exists but doesn't equal itemstack then return
  if use_result ~= nil and use_result ~= itemstack then
    return itemstack
  end
  -- pointing at an entity with an on_rightclick function
  if type(pointed_thing) == "table" and pointed_thing.ref then
    local entity = pointed_thing.ref:get_luaentity()
    if entity then
      local entity_use_function = entity[tph_eating.entity_use_function]
      -- entity has a function according to entity_use_function, send stuff to it! (self, player, itemstack, pointed_thing)
      if type(entity_use_function) == "function" then
        local stored_stack = ItemStack(itemstack:to_string())
        use_result = entity[tph_eating.entity_use_function](entity, user, itemstack, pointed_thing)
        -- we got an itemstack return, compare!
        if type(use_result) == "userdata" and use_result["get_name"] and use_result["get_count"] then
          if use_result:get_name() ~= stored_stack:get_name() or use_result:get_count() ~= stored_stack:get_count() then
            return use_result
          end
        elseif use_result ~= false then
          return itemstack
        end
      end
    end
  end
  if not players_eating[user:get_player_name()] then
    eating(user, itemstack)
  end
  return itemstack
end
tph_eating.on_main_use = on_main_use

-- this will automatically add proper usage of the eating_func function
-- will not touch tph_eating_no_edit items
function tph_eating.add_eating_hook(item,forcereplace,success_function)
  item = get_def(item)
  -- can't append
  if type(item) ~= "table" then
    return
  end
  if item.groups and item.groups.tph_eating_no_edit then
    return
  end
  -- run potential errors
  if type(tph_eating.use_function) ~= "string" then
    error(debug.traceback("tph_eating: 'use_function' should be string for indexing, got type: "..type(tph_eating.use_function),2))
  end
  if type(tph_eating.use_key) ~= "string" then
    error(debug.traceback("tph_eating: 'use_key' should be string for indexing, got type: "..type(tph_eating.use_key),2))
  end
  -- get old functions to run
  local oldfunc = item[tph_eating.use_function]
  local node_oldfunc = item["on_place"]
  minetest.override_item(item.name,{
    [tph_eating.use_function] = function(itemstack, player, pointed_thing)
      -- 4th parameter being previous old function, if forcereplace then do not use
      return on_main_use(itemstack, player, pointed_thing, (not forcereplace) and oldfunc)
    end,
    tph_eating_success = type(success_function) == "function" and success_function or nil
  })
end
-- PRIMARY FUNCTIONS /\

local function set_settings()
  tph_eating.burping = minetest.settings:get_bool("tph_eating.burping",tph_eating.burping)
  tph_eating.silly = minetest.settings:get_bool("tph_eating.silly",tph_eating.silly)
  tph_eating.eating_item_entity = minetest.settings:get_bool("tph_eating.eating_item_entity",tph_eating.eating_item_entity)
  tph_eating.burp_chance = tonumber(minetest.settings:get("tph_eating.burp_chance")) or tph_eating.burp_chance
  if not v540 then
    tph_eating.eating_item_entity = false
  end
end
set_settings() -- set initially (prior to mod load)

-- MOD SUPPORT
dofile(minetest.get_modpath("tph_eating").."/compatibilities.lua") -- run compatibility

minetest.register_on_mods_loaded(function()
  -- set again after mod load - players deserve what they want!
  set_settings()
end)

-- do not register entity if version is older than 5.4.0
if v540 then
  -- check tph_eating.eating_item_entity for whether or not this gets used. Gets defined regardless of false or true
  minetest.register_entity("tph_eating:eating_item",{
    initial_properties = {
      pointable = false,
      physical = false,
      static_save = false, -- I erase upon chunk unloading
      makes_footstep_sound = false,
      visual = "wielditem",
      visual_size = {x = 0.17, y = 0.17},
    },
    on_activate = function(self, staticdata, dtime_s)
      if staticdata == "" then
        -- I have no texture and so I don't want to spawn
        self.object:remove()
        return
      end
      local props = self.object:get_properties()
      props.wield_item = staticdata
      self.object:set_properties(props)
    end,
  })
end