A mod that provides eating functionality similar to a certain popular voxel game! Enjoy particles, sounds, and a delayed period of time with each nom!

LICENSING:
license of media
-------------------
(C) TPH/tph9677/TubberPupperHusker/TubberPupper/Damotrix <damotrixrob@gmail.com> 
tph_eating_chew(1-2), tph_eating_burp(1-3) marked with CC0 1.0 Universal
https://creativecommons.org/publicdomain/zero/1.0/
-------------------
license of code
-------------------
(C) TPH/tph9677/TubberPupperHusker/TubberPupper/Damotrix <damotrixrob@gmail.com> 
(GPLv3+)
https://www.gnu.org/licenses/licenses.html#GPL
Or see LICENSE.txt
