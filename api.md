# TPH_EATING ::

# SETTINGS (under global `tph_eating`)
* number `eating_time`
--
how many seconds it takes between each nom (minimum of 0.001sec). Default is `0.5`sec however can be modified by any mods

* number `eating_repeats`
--
how many times it repeats the eating() function (minimum of 2). Default is `4` however can be modified

* string `use_key`
--
what key provided in player:get_player_control() is to be used for input-holding. Default is `"RMB"`

* string `use_function`
--
what index in an item definition to override for eating functionality. Default is `"on_secondary_use"`

* string `entity_use_function`
--
if player is pointing at an entity, will run the provided function if it exists within the entity instead of eating. If entity function returns `false` or the player's ItemStack is unaffected then the player will continue to eat as normal. Eating function will send player, itemstack, and pointed_thing as arguments to the entity use function. Default is `"on_rightclick"`

* boolean `burping`
--
whether or not it should play the burping sound for the finishing sound (otherwise gets a variation of `eating_sound`). Does not override custom item definition sounds unless `eating_finished` contains "burp" in the name. Default is `true`

* number `burp_chance`
--
can be from 0 (or any decimal) to 100 to determine whether or not to play the burping sound. Default is `100`

* boolean `silly`
--
whether or not it should add eating functions for items that should otherwise not be edible (only used in MTG). Default is `false`

* boolean `eating_item_entity`
--
whether or not to show an entity attached to the player's "Head" bone when eating (wielditem). Default is `true`

* table/function `eating_sound`
--
what DEFAULT sound to play when a player is eating food, accepts sound_def from a function result

* table/function `finished_sound`
--
what DEFAULT sound to play when a player is finished eating food, accepts sound_def from a function result


NOTE: `tph_eating.eating_sound` and `tph_eating.finished_sound` will NOT override sounds provided in item definitions



# ITEMSTACK CONSUMPTION CUSTOMIZATION
* tph_eating_image (STRING/TABLE)
--
what image should be used instead of an ItemStack's `inventory_image` or node's `tiles` for consumption particles
if string, should be an appropriate file type (used by MT engine, e.g. 'png')
can be a table of strings with appropriate file types (randomly chosen)
if named 'tiles', then will use the node's tiles if provided

* tph_eating_repeats (NUMBER)
--
related to global `eating_repeats` however determined by the Itemstack locally instead

* tph_eating_itemstack_entity_override (STRING)
--
name of an itemstack to use as an eating model instead for the itemstack entity (what will show particles coming out of, what will be animated infront of the player)

* sounds.eating_chew (TABLE/FUNCTION)
--
related to global `eating_sound`. Accepts sound table or a sound table from a function

* sounds.eating_finished (TABLE/FUNCTION)
--
related to global `eating_finished`. Accepts sound table or a sound table from a function

* groups.tph_eating_no_edit (NUMBER)
--
used to determine whether or not to automatically set compatibility for the ItemStack 
checked in tph_eating.on_use_override and tph_eating.add_eating_hook


# API FUNCTIONS
* get_eating_information(itemdef, noerror)
--
itemdef can be `ItemStack`, an item definition table, or string. `noerror` prevents runtime from stopping due to being unable to find item definition
returns a table containing `eating_time` number, `eating_repeats` number, `eating_sound` table, `finshed_sound` table


* cease_eating(player)
--
player can be string in reference to a player's name
stop a player from continuing to eat if they are eating
sets boolean `force_finish` to true in provided player's eating data
will run callback `tph_eating_failed` if provided


* get_player_eating_data(player)
--
player can be string in reference to a player's name
returns the mod's data on the player eating or nil if there is no data (player is not eating)
table of several variables: `iteration` (how many minetest.after() iterations have passed), `tool_def` (ItemStack properties), `image_list` (table of expected strings used to determine `image`) `image` (image used for food particles, modified on each iteration), `eating_info` (get_eating_information return), `index` (wielded_item index), `height` (player height, used for particle pos)
variables only present if `eating_item_entity` is `true` and version is `5.4.0+`: `obj` (entity created for ItemStack near mouth), `hud_wielditem` (hud setting, DO NOT MODIFY), `item_pos` (for `obj`'s relative position)
all above variables can be modified in `tph_eating_initiated` and `tph_eating_ongoing` callbacks


* add_eating_hook(itemdef, forcereplace, success_function)
--
itemdef can be string (name) or table. `forcereplace` will be a boolean that indicates whether or not to erase the old `use_function` if found, default is false. `success_function` if specified, can be a function that is ran upon `tph_eating_success` callback
adds tph_eating functionality to the provided item
if not `forcereplace`, will run the old `use_function`, if nil or the ItemStack isn't returned, will not begin eating
use callback `_eating_success` in definition to determine what should be done upon success OR define a function to be ran by the `tph_eating_success` callback with third parameter `success_function`


* on_use_override(def,siic)
--
`def` can be a definition table (must have a name string inside) or name string
overrides a provided edible item/node's definition to work accordingly to tph_eating
simply automates running the old on_use function in `tph_eating_success` callback and erases it from definition
if a table; will add any modifications to sounds, `tph_eating_image`, or `tph_eating_repeats` (will not save any other modified values)
`siic` is 'SaveItemstackInCreative" and expected boolean. Will add 1 count to an ItemStack to prevent depletion if player is considered to be in creative by `tph_eating.player_in_creative`





# BASE API FUNCTIONS
* player_in_creative(player)
--
checks if a `player` object or name is in creative returning true if true or false otherwise checks the creative cache or if player has the "creative" privilege
can be replaced with a new function by setting `tph_eating.player_in_creative` to whatever function you would desire for checking a player's creative privileges

* play_sound(sound_def,pos)
--
plays a specified `sound_def` table or any of its numbered indexes at a given `pos` table (or globally if not given)
by "numbered indexes", sound_table = {1={sound_def},2={sound_def},3={sound_def}) would be valid and would have a chance of playing either of those sounds. If name is not provided in those indexes, the sound_table's "name" if provided will be used e.g. sound_table = {name="sound",1={sound_def}} and so on would be valid (will NOT override provided names in the indexes)
will randomize any two number value tables {1,5} between its values e.g. a pitch of {0.9,1.5} would play the sound at a random pitch between both numbers (decimal included)


* eating_func(player, itemstack)
--
`player` must be a player object and `itemstack` must be an ItemStack 
the primary function to begin input-holding consumption
not recommended to directly use unless you wish to specify more than one or differing functions than the `use_function` for eating
named `eating` in base code however presents as `eating_func` in the global table


* clear_eating(player)
--
player can be string in reference to a player's name
remove player's information from local players_eating table


# API CALLBACKS
* tph_eating_condition(player, itemstack)
--
`player` will be a player obj. `itemstack` will be an ItemStack
if specified, expects returned boolean `true` to continue with eating, otherwise will suspect `false` and will not proceed with eating
use this to determine whether or not a player should begin to eat (too full, unable to consume, etc etc)

* tph_eating_initiated(player, itemstack, data)
--
`player` will be a player obj. `itemstack` will be an ItemStack. `data` will be a table of temporary information for the player currently eating.
ran when the player begins eating
expects nil or ItemStack in return, otherwise will cease eating if returned something invalid
ran after `tph_eating_condition` if player can successfully begin eating
get and modify necessary values from `data` (see `get_player_eating_data`)

* tph_eating_ongoing(player, itemstack, data)
--
`player` will be a player obj. `itemstack` will be an ItemStack. `data` will be a table of temporary information for the player currently eating.
runs every repeated eating iteration (except for beginning and end of eating)
expects nil or ItemStack in return, otherwise will cease eating if returned something invalid
execute anything that should happen while the player is eating (effects, text messages, animations, etc)
get and modify necessary values from `data` (see `get_player_eating_data`)

* tph_eating_failed(player, itemstack, data)
--
`player` will be a player obj. `itemstack` will be an ItemStack. `data` will be a table of temporary information for the player currently eating.
ran if the player stops eating (usually due to not holding `use_key`, no longer holding the ItemStack, or switching inventory slots)
get necessary values from `data` (see `get_player_eating_data`)

* tph_eating_success(player, itemstack, data)
--
`player` will be a player obj. `itemstack` will be an ItemStack. `data` will be a table of temporary information for the player currently eating.
ran if the player successfully finishes eating
use this function to determine what should happen to the player when they successfully finish (satiety, hydration, etc)
RETURN ItemStack if you manually modify it in eating_success (you will need to manually take 1 count away)
get necessary values from `data` (see `get_player_eating_data`)